using PageUpInterview.Common.ViewModels;

namespace PageUpInterview.Common.Rules
{
  public class VolumnRule : ParcelRuleBase
  {
    public override bool IsApplicableToParcel(ParcelInfo parcel)
    {
      return parcel.GetVolumn() < this.Threshhold;
    }

    public override decimal CalculateParcelCost(ParcelInfo parcel)
    {
      var volumn = parcel.GetVolumn();
      return this.UnitPrice * volumn;
    }
  }
}