using System;
using PageUpInterview.Common.ViewModels;

namespace PageUpInterview.Common.Rules
{
  public class RejectRule : WeightRule
  {
    public RejectRule()
    {
      this.Threshhold = 50;
    }

    public override decimal CalculateParcelCost(ParcelInfo parcel)
    {
      return -1;
    }
  }
}