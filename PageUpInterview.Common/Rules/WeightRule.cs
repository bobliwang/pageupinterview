using PageUpInterview.Common.ViewModels;

namespace PageUpInterview.Common.Rules
{
  public class WeightRule : ParcelRuleBase
  {
    public override bool IsApplicableToParcel(ParcelInfo parcel)
    {
      return parcel.Weight > this.Threshhold;
    }

    public override decimal CalculateParcelCost(ParcelInfo parcel)
    {
      return this.UnitPrice * parcel.Weight;
    }
  }
}