using PageUpInterview.Common.ViewModels;

namespace PageUpInterview.Common.Rules
{
  public interface IParcelRule
  {
    int Priority { get; }

    string RuleName { get; }

    bool IsApplicableToParcel(ParcelInfo parcel);

    decimal CalculateParcelCost(ParcelInfo parcel);
  }
}