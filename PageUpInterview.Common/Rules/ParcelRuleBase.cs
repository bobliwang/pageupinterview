﻿using PageUpInterview.Common.ViewModels;

namespace PageUpInterview.Common.Rules
{
  public abstract class ParcelRuleBase : IParcelRule
  {
    public int Priority { get; set; }

    public string RuleName { get; set; }

    public double Threshhold { get; set; }

    public decimal UnitPrice { get; set; }

    public abstract bool IsApplicableToParcel(ParcelInfo parcel);

    public abstract decimal CalculateParcelCost(ParcelInfo parcel);
  }
}