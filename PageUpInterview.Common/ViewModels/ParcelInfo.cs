﻿using Newtonsoft.Json;

namespace PageUpInterview.Common.ViewModels
{
  /// <summary>
  /// According to the interview question, the four user inputs are four integers as
  ///   1. Weight (kg)
  ///   2. Height (cm)
  ///   3. Width (cm)
  ///   4. Depth (cm)
  /// Though I think double type will reflect the real business scenarios better, I will keep it to the interview question requirements.
  /// </summary>
  public class ParcelInfo
  {
    [JsonProperty("weight")]
    public int Weight { get; set; }

    [JsonProperty("height")]
    public int Height { get; set; }

    [JsonProperty("width")]
    public int Width { get; set; }

    [JsonProperty("depth")]
    public int Depth { get; set; }

    public int GetVolumn()
    {
      return this.Height*this.Width*this.Depth;
    }
  }
}