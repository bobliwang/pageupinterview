﻿using Newtonsoft.Json;

namespace PageUpInterview.Common.ViewModels
{
  public class DeliveryCost
  {
    [JsonProperty("ruleName")]
    public string RuleName { get; set; }

    [JsonProperty("postage")]
    public decimal Postage { get; set; }
  }
}