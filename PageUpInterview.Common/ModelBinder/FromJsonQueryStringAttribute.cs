﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Newtonsoft.Json;

namespace PageUpInterview.Common.ModelBinder
{
  public class FromJsonQueryStringAttribute : ActionFilterAttribute
  {
    private readonly JsonSerializerSettings _jsonSerializerSettings;

    public FromJsonQueryStringAttribute()
    {
      _jsonSerializerSettings = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;
    }

    public override void OnActionExecuting(HttpActionContext actionContext)
    {
      var parameters = actionContext.ActionDescriptor.GetParameters().ToList();
      var queryParameters = actionContext.Request.RequestUri.ParseQueryString();

      foreach (var param in parameters)
      {
        var json = queryParameters[param.ParameterName];

        if (json != null)
        {
          var paramVal = JsonConvert.DeserializeObject(json, param.ParameterType, _jsonSerializerSettings);
          actionContext.ActionArguments[param.ParameterName] = paramVal;
        }
      }
    }
  }
}