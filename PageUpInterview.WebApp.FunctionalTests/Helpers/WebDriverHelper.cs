﻿using System;
using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace PageUpInterview.WebApp.FunctionalTests.Helpers
{
  public class WebDriverHelper
  {
    private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    private readonly RemoteWebDriver _webDriver;

    public string BaseUrl { get; private set; }


    public WebDriverHelper(RemoteWebDriver webDriver, string url)
    {
      _webDriver = webDriver;
      this.BaseUrl = url;
    }

    public WebDriverWait Wait(TimeSpan timeSpan)
    {
      return new WebDriverWait(_webDriver, timeSpan);
    }

    public WebDriverHelper GoToUrl(string url)
    {
      logger.Info("Going to url: " + url);
      _webDriver.Navigate().GoToUrl(url);
      
      return this;
    }

    public WebDriverHelper GoToModule(string moduleName)
    {

      var url = this.GetModuleUrl(moduleName);

      return this.GoToUrl(url);
    }

    public WebDriverHelper SetTextInNgModel(string text, string ngModel, string elementName = "input")
    {
      var element = this.FindElementByNgModel(ngModel, elementName);

      element.Clear();
      element.SendKeys(text);

      return this;
    }

    public WebDriverHelper AppendTextInNgModel(string text, string ngModel, string elementName = "input")
    {
      var element = this.FindElementByNgModel(ngModel, elementName);

      element.SendKeys(text);

      return this;
    }


    public WebDriverHelper ClickByNgClick(string ngClick, string elementName = "button")
    {
      var element = this.FindElementByNgClick(ngClick, elementName);

      element.Click();

      return this;
    }

    public IWebElement FindElementByNgModel(string ngModel, string elementName = "input")
    {
      return this.FindElementByAttribute("ng-model", ngModel, elementName);
    }

    public IWebElement FindElementByNgClick(string ngClick, string elementName = "button")
    {
      return this.FindElementByAttribute("ng-click", ngClick, elementName);
    }

    public IWebElement FindElementByAttribute(string attribute, string value, string elementName)
    {
      return this.FindElementByXPath(string.Format("//{0}[@{1}='{2}']", elementName, attribute, value));
    }

    public IWebElement FindElementByXPath(string xPath)
    {
      try
      {
        return this.Wait(TimeSpan.FromSeconds(3)).Until(driver => _webDriver.FindElementByXPath(xPath));
      }
      catch (Exception ex)
      {
        throw new Exception("Unable to find element by path:" + xPath, ex);
      }
    }


    protected string GetModuleUrl(string moduleName)
    {
      return string.Format("{0}/modules/{1}/index.cshtml", BaseUrl, moduleName);
    }

    public WebDriverHelper Click(string xPath)
    {
      this.FindElementByXPath(xPath).Click();

      return this;
    }

  }

  public static class WebDriverExtensions
  {
    public static WebDriverHelper Helper(this RemoteWebDriver webDriver, string urlBase)
    {
      return new WebDriverHelper(webDriver, urlBase);
    }
  }
}