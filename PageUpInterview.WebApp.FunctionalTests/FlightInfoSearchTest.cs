﻿using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PageUpInterview.WebApp.FunctionalTests.Helpers;

namespace PageUpInterview.WebApp.FunctionalTests
{
  [TestClass]
  public class FlightInfoSearchTest
  {
    private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    /// <summary>
    /// The url base to local iis express web app folder.
    /// </summary>
    private readonly string _urlBase = "http://localhost:2753/apps";

    private ChromeDriver _chrome;

    [TestMethod]
    public void ParcelHeavierThan50Kg_ShouldBeRejected()
    {
      using (_chrome = new ChromeDriver())
      {
        var element = _chrome.Helper(_urlBase)
          .GoToModule("parcel")
          .SetTextInNgModel("51", "parcelInfo.weight")
          .SetTextInNgModel("2", "parcelInfo.height")
          .SetTextInNgModel("3", "parcelInfo.width")
          .SetTextInNgModel("4", "parcelInfo.depth")
          .ClickByNgClick("calculateDeliveryCost()")
          .FindElementByXPath("//span[@id='rejectspan']");

        Assert.IsNotNull(element);
      }
    }
  }
}
