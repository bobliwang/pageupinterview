﻿using System;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using log4net;

namespace PageUpInterview.WebApp
{
  public class Global : System.Web.HttpApplication
  {
    private static readonly ILog Logger;

    static Global()
    {
      log4net.Config.XmlConfigurator.Configure();
      Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }



    protected void Application_Start(object sender, EventArgs e)
    {
      try
      {
        var builder = new ContainerBuilder();

        var dependencyConfig = new DependencyConfig();
        var container = dependencyConfig.RegisterDependncies(builder);

        // Configure Web API with the dependency resolver.
        var webApiResolver = new AutofacWebApiDependencyResolver(container);
        GlobalConfiguration.Configuration.DependencyResolver = webApiResolver;


        WebApiConfig.Register(GlobalConfiguration.Configuration);
      }
      catch (Exception ex)
      {
        Logger.Fatal(string.Format("Failed to start application! ExceptionType: {0}, Message: {1}, StackTrace: {2}", ex.GetType().FullName, ex.Message, ex.StackTrace));
      }
    }

    protected void Session_Start(object sender, EventArgs e)
    {

    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {

    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {

    }

    protected void Application_Error(object sender, EventArgs e)
    {

    }

    protected void Session_End(object sender, EventArgs e)
    {

    }

    protected void Application_End(object sender, EventArgs e)
    {

    }
  }
}