﻿using System.Collections.Generic;
using System.Configuration;
using Autofac;
using Autofac.Integration.WebApi;
using PageUpInterview.Common.Rules;
using PageUpInterview.Services;
using PageUpInterview.Services.Interfaces;

namespace PageUpInterview.WebApp
{
  public class DependencyConfig
  {
    public IContainer RegisterDependncies(ContainerBuilder builder)
    {
      var deliveryCostCostCalculator = new DeliveryCostCalculator(new ParcelRuleProvider());

      builder.RegisterInstance(deliveryCostCostCalculator)
             .As<IDeliveryCostCalculator>()
             .SingleInstance();


      var globalAssembly = typeof(Global).Assembly;
      builder.RegisterApiControllers(globalAssembly);

      var container = builder.Build();

      return container;
    }
  }
}