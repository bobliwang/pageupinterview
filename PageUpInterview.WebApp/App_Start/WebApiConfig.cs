﻿using System.Web.Http;
using System.Web.Http.Filters;

namespace PageUpInterview.WebApp
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      ////config.MapHttpAttributeRoutes();

      config.Routes.MapHttpRoute(
          name: "FlightsApi",
          routeTemplate: "api/FlightProxy",
          defaults: new { controller = "ApiFlightProxy", action = "GetFlightInfos" }
      );

      config.Routes.MapHttpRoute(
          name: "DefaultApi",
          routeTemplate: "api/{controller}/{action}/{id}",
          defaults: new { id = RouteParameter.Optional }
      );

      RegisterWebApiFilters(GlobalConfiguration.Configuration.Filters);
    }

    public static void RegisterWebApiFilters(HttpFilterCollection filters)
    {
      
    }
  }
}
