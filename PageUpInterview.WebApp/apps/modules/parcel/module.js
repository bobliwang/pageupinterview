﻿// the module
angular.module('parcel', ['my.ui.datepicker', 'NgSwitchery', 'ngRoute', 'ui.bootstrap'])
  // inject deliveryCostService here.
  .factory('deliveryCostService', ["$http", "$log", function ($http, $log) {
    return {
      calculateDeliveryCost: function (parcelInfo) {
        return $http.get("/api/ApiDeliveryCost/CalculateDeliveryCost", {
          cache: false, // no cache
          params: { parcelInfo: parcelInfo }
        });
      }
    };
  }])
  // routing config.
  .config(["$routeProvider", "$locationProvider", function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
      controller: 'ParcelCtrl',
      templateUrl: 'list.cshtml'
    }).otherwise({
      redirectTo: '/'
    });
  }])
  .directive('numberOnly', function () {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function (scope, element, attrs, ngModel) {
        if (!ngModel) return;
        ngModel.$parsers.unshift(function (inputValue) {
          var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' ' || s == '.'); }).join('');
          ngModel.$viewValue = digits;
          ngModel.$render();
          return digits;
        });
      }
    };
  })
  // controller
  .controller('ParcelCtrl', ["$scope", "$http", "$timeout", "$log", "deliveryCostService", function ($scope, $http, $timeout, $log, deliveryCostService) {

    // initializer
    function init() {
      $scope.parcelInfo = {};
    }

    // get flight info.
    $scope.calculateDeliveryCost = function () {

      if (!validate()) {
        return;
      }

      preSendSearchRequest();

      deliveryCostService.calculateDeliveryCost($scope.parcelInfo)
        .success(function(data, status, headers, config) {
          onDeliveryCostSuccess(data, status, headers, config);
        })
        .error(function (data, status, headers, config) {
          onDeliveryCostError(data, status, headers, config);
        })
      ;
    };

    // parameter validation
    function validate() {

      $scope.validationErrors = [];
      
      if ($scope.parcelInfo.weight == null) {
        $scope.validationErrors.push("Weight is required.");
      }

      if ($scope.parcelInfo.height == null) {
        $scope.validationErrors.push("Height is required.");
      }

      if ($scope.parcelInfo.width == null) {
        $scope.validationErrors.push("Width is required.");
      }

      if ($scope.parcelInfo.depth == null) {
        $scope.validationErrors.push("Depth is required.");
      }
      

      return $scope.validationErrors.length == 0;
    }

    // callback for a success
    function onDeliveryCostSuccess(data, status, headers, config) {
      $scope.deliveryCost = data;
      $scope.isInProgress = false;
    }

    // callback for a failure
    function onDeliveryCostError(data, status, headers, config) {
      $scope.isInProgress = false;
      $scope.errorMsg = "Cost calculation failed.";

      $log.error("error", {
        data: data,
        status: status,
        headers: headers,
        config: config
      });
    }

    // prepare search
    function preSendSearchRequest() {

      $scope.deliveryCost = {};

      // clear error msg
      $scope.errorMsg = null;
      
      // set isInProgress to true - disable the calc button
      $scope.isInProgress = true;
    }


    // call initalizer
    init();
}]);