﻿using System.Collections.Generic;
using System.Web.Http;
using PageUpInterview.Common.ModelBinder;
using PageUpInterview.Common.ViewModels;
using PageUpInterview.Services.Interfaces;

namespace PageUpInterview.WebApp.Controllers.Api
{
  public class ApiDeliveryCostController : ApiController
  {
    private readonly IDeliveryCostCalculator _deliveryCostCalculator;

    public ApiDeliveryCostController(IDeliveryCostCalculator deliveryCostCalculator)
    {
      _deliveryCostCalculator = deliveryCostCalculator;
    }

    [HttpGet]
    [FromJsonQueryString]
    public DeliveryCost CalculateDeliveryCost(ParcelInfo parcelInfo)
    {
      return _deliveryCostCalculator.Calculate(parcelInfo);
    }
  }
}