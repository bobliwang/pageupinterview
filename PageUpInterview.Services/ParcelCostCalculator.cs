﻿using System;
using System.Collections.Generic;
using System.Linq;
using PageUpInterview.Common.Rules;
using PageUpInterview.Common.ViewModels;
using PageUpInterview.Services.Interfaces;

namespace PageUpInterview.Services
{
  public class DeliveryCostCalculator : IDeliveryCostCalculator
  {
    public DeliveryCostCalculator(IParcelRuleProvider parcelRuleProvider)
    {
      this.Rules = parcelRuleProvider.GetParcelCostRules();
    }

    public DeliveryCostCalculator(IEnumerable<IParcelRule> rules)
    {
      // sort the rules by priority.
      this.Rules = rules.OrderBy(x => x.Priority).ToList();
    }

    public IList<IParcelRule> Rules { get; private set; }

    public DeliveryCost Calculate(ParcelInfo parcel)
    {
      var rule = this.Rules.FirstOrDefault(x => x.IsApplicableToParcel(parcel));

      if (rule == null)
      {
        throw new Exception("No rule is found! Please check if the rules are configured correctly.");
      }

      var deliveryCost = new DeliveryCost();

      deliveryCost.RuleName = rule.RuleName;
      deliveryCost.Postage = rule.CalculateParcelCost(parcel);

      return deliveryCost;
    }
  }
}