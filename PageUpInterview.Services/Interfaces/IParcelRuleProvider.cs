﻿using System.Collections.Generic;
using PageUpInterview.Common.Rules;

namespace PageUpInterview.Services.Interfaces
{
  public interface IParcelRuleProvider
  {
    IList<IParcelRule> GetParcelCostRules();
  }
}