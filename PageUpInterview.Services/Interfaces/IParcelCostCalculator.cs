﻿using PageUpInterview.Common.ViewModels;

namespace PageUpInterview.Services.Interfaces
{
  public interface IDeliveryCostCalculator
  {
    DeliveryCost Calculate(ParcelInfo parcel);
  }
}