using System.Collections.Generic;
using System.Linq;
using PageUpInterview.Common.Rules;
using PageUpInterview.Services.Interfaces;

namespace PageUpInterview.Services
{
  public class ParcelRuleProvider : IParcelRuleProvider
  {
    public IList<IParcelRule> GetParcelCostRules()
    {
      var rules = new List<IParcelRule>();

      rules.Add(new RejectRule { Priority = 1, Threshhold = 50, RuleName = "Reject" });
      rules.Add(new WeightRule { Priority = 2, Threshhold = 10, RuleName = "Heavy Parcel", UnitPrice = 15 });
      rules.Add(new VolumnRule { Priority = 3, Threshhold = 1500, RuleName = "Small Parcel", UnitPrice = (decimal)0.05 });
      rules.Add(new VolumnRule { Priority = 4, Threshhold = 2500, RuleName = "Medium Parcel", UnitPrice = (decimal)0.04 });
      rules.Add(new VolumnRule { Priority = 5, Threshhold = double.MaxValue, RuleName = "Large Parcel", UnitPrice = (decimal)0.03 });

      return rules.OrderBy(x => x.Priority).ToList();
    }
  }
}